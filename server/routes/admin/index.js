module.exports = app => {
    const express = require('express')

    const jwt = require('jsonwebtoken')
    const assert = require('http-assert')
    const AdminUser  = require('../../models/AdminUser')

    const router = express.Router({
        mergeParams: true  //合并url参数，父级url参数都能合并到Router里面路由来
    })
    // const Category = require('../../models/Category')

//创建资源 
    router.post('/', async(req, res) => {
        const model = await req.Model.create(req.body)
        res.send(model)
    })
//更新资源
    router.put('/:id', async(req, res) => {
        const model = await req.Model.findByIdAndUpdate(req.params.id, req.body)
        res.send(model)
    })
//删除资源
    router.delete('/:id', async(req, res) => {
        await req.Model.findByIdAndDelete(req.params.id, req.body)
        res.send({
            success: true
        })
    })
//资源列表接口
    router.get('/', async(req, res) => {
        const queryOptions = {}
        if(req.Model.modelName === "Category") {
            queryOptions.populate = 'parent'
        }
        const items = await req.Model.find().setOptions(queryOptions).limit(10)
        res.send(items)
    })
//资源详情
    router.get('/:id', async(req, res) => {
        const model = await req.Model.findById(req.params.id)
        res.send(model)
    })

    //登陆校验中间件
    const authMiddleware =  require('../../middleware/auth')
    //资源中间件 
    const resourceMiddleware = require('../../middleware/resource')

    app.use('/admin/api/rest/:resource', authMiddleware(), resourceMiddleware(), router)


const multer = require('multer')
const upload = multer({dest:__dirname + '/../../uploads'})
app.post('/admin/api/upload', authMiddleware(), upload.single('file'), async (req, res) => {
    const file = req.file
    file.url = `http://localhost:3000/uploads/${file.filename}`
    res.send(file)
    })

app.post('/admin/api/login', async (req, res) => {
    const {username, password}  = req.body
    //用户登陆步骤：1根据用户名找用户 
    const user = await AdminUser.findOne({username}).select('+password')
    // if(!user) {
    //     return res.status(422).send({
    //         message:'用户不存在'
    //     })
    // }
    assert(user, 422, '用户不存在') // assert 插件代替以上注释报错（1查找对象，2不存在报错状态码，3返回报错信息）

    //2校验密码 
    const isValid = require('bcryptjs').compareSync(password, user.password) //compareSync 检验明文和密文是否匹配
    // if(!isValid) {
    //     return res.status(422).send({
    //         message:'密码错误'
    //     }) 
    // }
    assert(isValid, 422, '密码错误')// assert 插件代替以上注释报错（1查找对象，2不存在报错状态码，3返回报错信息）

    //3返回token  :  sudo npm i jsonwebtoken
    const token = jwt.sign({ id:user._id }, app.get('secret'))
        res.send({token})
    })

//assert 错误处理函数
    app.use(async ( err,req, res, next) => {
        res.status(err.statusCode || 500).send({
            message: err.message
        })
    })

}