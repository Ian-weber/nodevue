
const mongoose = require('mongoose')
const schema = new mongoose.Schema({
    username: { type: String },
    password: { 
        type: String, 
        select:false,  //密码散列后再修改会对散列值再次散列，不显示防止再次散列
         set(val) {
            //服务端 sudo npm i --save bcryptjs 将密码不可逆散列加密
            return require('bcryptjs').hashSync(val, 12) //（password, 位数:越大越安全越耗时） 
        } 
    
    },

}) 

module.exports = mongoose.model('AdminUser', schema)